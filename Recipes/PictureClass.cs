﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace Recipes
{
    public class PictureClass
    {
        public static DataSet ExecuteQuery(string cmd)
        {
            SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
            DataSet myDataSet = null;

            try
            {
                connection.Open();
                myDataSet = new DataSet();
                SqlDataAdapter myDataAdapter = new SqlDataAdapter(cmd, connection);
                myDataAdapter.Fill(myDataSet);
                connection.Close();

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            return myDataSet;
        }
    }
}
