﻿namespace Recipes
{
    partial class Recipes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Recipes));
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.bntnInsert = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBoxRecipes = new System.Windows.Forms.GroupBox();
            this.btnImage = new System.Windows.Forms.Button();
            this.txtCategoryID = new System.Windows.Forms.TextBox();
            this.txtPrice = new System.Windows.Forms.MaskedTextBox();
            this.pbPicture = new System.Windows.Forms.PictureBox();
            this.comboDifficulty = new System.Windows.Forms.ComboBox();
            this.txtRecipeDescription = new System.Windows.Forms.RichTextBox();
            this.txtRecipeName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxOptions.SuspendLayout();
            this.groupBoxRecipes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOptions.Controls.Add(this.btnClose);
            this.groupBoxOptions.Controls.Add(this.btnUpdate);
            this.groupBoxOptions.Controls.Add(this.bntnInsert);
            this.groupBoxOptions.Controls.Add(this.btnNew);
            this.groupBoxOptions.Font = new System.Drawing.Font("Tw Cen MT", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOptions.ForeColor = System.Drawing.Color.Navy;
            this.groupBoxOptions.Location = new System.Drawing.Point(12, 269);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(302, 158);
            this.groupBoxOptions.TabIndex = 0;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options...";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.PeachPuff;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(103, 106);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 44);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.PeachPuff;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(202, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(72, 44);
            this.btnUpdate.TabIndex = 2;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // bntnInsert
            // 
            this.bntnInsert.BackColor = System.Drawing.Color.PeachPuff;
            this.bntnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bntnInsert.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntnInsert.Location = new System.Drawing.Point(105, 47);
            this.bntnInsert.Name = "bntnInsert";
            this.bntnInsert.Size = new System.Drawing.Size(69, 44);
            this.bntnInsert.TabIndex = 1;
            this.bntnInsert.Text = "Insert";
            this.bntnInsert.UseVisualStyleBackColor = false;
            this.bntnInsert.Click += new System.EventHandler(this.BntnInsert_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.PeachPuff;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(6, 47);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(71, 44);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // groupBoxRecipes
            // 
            this.groupBoxRecipes.BackColor = System.Drawing.Color.PeachPuff;
            this.groupBoxRecipes.Controls.Add(this.btnImage);
            this.groupBoxRecipes.Controls.Add(this.txtCategoryID);
            this.groupBoxRecipes.Controls.Add(this.txtPrice);
            this.groupBoxRecipes.Controls.Add(this.pbPicture);
            this.groupBoxRecipes.Controls.Add(this.comboDifficulty);
            this.groupBoxRecipes.Controls.Add(this.txtRecipeDescription);
            this.groupBoxRecipes.Controls.Add(this.txtRecipeName);
            this.groupBoxRecipes.Controls.Add(this.label6);
            this.groupBoxRecipes.Controls.Add(this.label5);
            this.groupBoxRecipes.Controls.Add(this.label4);
            this.groupBoxRecipes.Controls.Add(this.label3);
            this.groupBoxRecipes.Controls.Add(this.label2);
            this.groupBoxRecipes.Controls.Add(this.label1);
            this.groupBoxRecipes.Font = new System.Drawing.Font("Tw Cen MT", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxRecipes.ForeColor = System.Drawing.Color.Navy;
            this.groupBoxRecipes.Location = new System.Drawing.Point(403, 35);
            this.groupBoxRecipes.Name = "groupBoxRecipes";
            this.groupBoxRecipes.Size = new System.Drawing.Size(385, 392);
            this.groupBoxRecipes.TabIndex = 1;
            this.groupBoxRecipes.TabStop = false;
            this.groupBoxRecipes.Text = "Recipes...";
            // 
            // btnImage
            // 
            this.btnImage.Location = new System.Drawing.Point(163, 294);
            this.btnImage.Name = "btnImage";
            this.btnImage.Size = new System.Drawing.Size(100, 27);
            this.btnImage.TabIndex = 15;
            this.btnImage.Text = "Get Image";
            this.btnImage.UseVisualStyleBackColor = true;
            this.btnImage.Click += new System.EventHandler(this.BtnImage_Click);
            // 
            // txtCategoryID
            // 
            this.txtCategoryID.Location = new System.Drawing.Point(98, 359);
            this.txtCategoryID.Name = "txtCategoryID";
            this.txtCategoryID.ReadOnly = true;
            this.txtCategoryID.Size = new System.Drawing.Size(29, 25);
            this.txtCategoryID.TabIndex = 14;
            this.txtCategoryID.Click += new System.EventHandler(this.TxtCategoryID_Click);
            this.txtCategoryID.TabIndexChanged += new System.EventHandler(this.TxtCategoryID_Click);
            this.txtCategoryID.Enter += new System.EventHandler(this.TxtCategoryID_Click);
            // 
            // txtPrice
            // 
            this.txtPrice.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtPrice.Location = new System.Drawing.Point(98, 327);
            this.txtPrice.Mask = "00.00";
            this.txtPrice.Name = "txtPrice";
            this.txtPrice.Size = new System.Drawing.Size(100, 25);
            this.txtPrice.TabIndex = 13;
            // 
            // pbPicture
            // 
            this.pbPicture.BackColor = System.Drawing.Color.Navy;
            this.pbPicture.Location = new System.Drawing.Point(77, 177);
            this.pbPicture.Name = "pbPicture";
            this.pbPicture.Size = new System.Drawing.Size(276, 116);
            this.pbPicture.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbPicture.TabIndex = 12;
            this.pbPicture.TabStop = false;
            // 
            // comboDifficulty
            // 
            this.comboDifficulty.BackColor = System.Drawing.SystemColors.HighlightText;
            this.comboDifficulty.FormattingEnabled = true;
            this.comboDifficulty.Items.AddRange(new object[] {
            "Simple",
            "Medium",
            "Complex"});
            this.comboDifficulty.Location = new System.Drawing.Point(131, 130);
            this.comboDifficulty.Name = "comboDifficulty";
            this.comboDifficulty.Size = new System.Drawing.Size(222, 27);
            this.comboDifficulty.TabIndex = 11;
            // 
            // txtRecipeDescription
            // 
            this.txtRecipeDescription.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtRecipeDescription.Location = new System.Drawing.Point(90, 64);
            this.txtRecipeDescription.Name = "txtRecipeDescription";
            this.txtRecipeDescription.Size = new System.Drawing.Size(263, 52);
            this.txtRecipeDescription.TabIndex = 10;
            this.txtRecipeDescription.Text = "";
            // 
            // txtRecipeName
            // 
            this.txtRecipeName.BackColor = System.Drawing.SystemColors.HighlightText;
            this.txtRecipeName.Location = new System.Drawing.Point(77, 28);
            this.txtRecipeName.Name = "txtRecipeName";
            this.txtRecipeName.Size = new System.Drawing.Size(276, 25);
            this.txtRecipeName.TabIndex = 8;
            // 
            // label6
            // 
            this.label6.AllowDrop = true;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 365);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(86, 19);
            this.label6.TabIndex = 7;
            this.label6.Text = "Category ID:";
            // 
            // label5
            // 
            this.label5.AllowDrop = true;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 333);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(75, 19);
            this.label5.TabIndex = 6;
            this.label5.Text = "Final Price:";
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Photo:";
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 133);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Level of Difficulty:";
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Description:";
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // Recipes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBoxRecipes);
            this.Controls.Add(this.groupBoxOptions);
            this.Name = "Recipes";
            this.Text = "Recipes";
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxRecipes.ResumeLayout(false);
            this.groupBoxRecipes.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button bntnInsert;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBoxRecipes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCategoryID;
        private System.Windows.Forms.MaskedTextBox txtPrice;
        private System.Windows.Forms.PictureBox pbPicture;
        private System.Windows.Forms.ComboBox comboDifficulty;
        private System.Windows.Forms.RichTextBox txtRecipeDescription;
        private System.Windows.Forms.TextBox txtRecipeName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImage;
    }
}