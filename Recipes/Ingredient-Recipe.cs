﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Recipes
{
    public partial class Ingredient_Recipe : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        int recordID;

        public Ingredient_Recipe()
        {
            InitializeComponent();
        }

        //Renewing form
        private void BtnNew_Click(object sender, EventArgs e)
        {
            comboRecipeNames.Text = "";
            txtRecipeID.Text = "";
            comboIngredientName.Text = "";
            txtIngredientID.Text = "";
        }

        //Inserting
        private void BntnInsert_Click(object sender, EventArgs e)
        {
            if (comboIngredientName.Text != "" && comboRecipeNames.Text != "")
            {
                int rID = RecipeID();
                txtRecipeID.Text = Convert.ToString(rID);
                int ingID = IngredientID();
                txtIngredientID.Text = Convert.ToString(ingID);

                DialogResult question = MessageBox.Show("Are you sure you want to insert this record?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO records(recipe_name, recipe_id, ingredient_id, ingredient_name) VALUES ('{comboRecipeNames.Text}', {txtRecipeID.Text}, {txtIngredientID.Text}, '{comboIngredientName.Text}')");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        bntnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Ingredient to Recipe record was successfully inserted!");
                    }
                    catch (SqlException)
                    {
                        string error = "An error occurred when performing the query or the record you requested to insert already exists.";
                        string title = "ERROR!";
                        MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;
                        DialogResult result = MessageBox.Show(error, title, buttons, MessageBoxIcon.Error);
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }


        //Updating
        private int GetRecordID()
        {
            int recordID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT record_id FROM records WHERE recipe_name = '{comboRecipeNames.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                recordID = Convert.ToInt16(table.Rows[0]["record_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return recordID;
        }
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (comboIngredientName.Text != "" && comboRecipeNames.Text != "")
            {
                int rID = RecipeID();
                txtRecipeID.Text = Convert.ToString(rID);
                int ingID = IngredientID();
                txtIngredientID.Text = Convert.ToString(ingID);
                recordID = GetRecordID();

                DialogResult question = MessageBox.Show($"Are you sure you want to update Record {recordID} with the {comboRecipeNames.Text} recipe?", "Update Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE records SET recipe_name = '{comboRecipeNames.Text}', recipe_id = {txtRecipeID.Text}, ingredient_name = '{comboIngredientName.Text}', ingredient_id = {txtIngredientID.Text} WHERE record_id = {recordID}");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Recipe successfully updated!");
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }


        //Loading the combo boxes
        private void Ingredient_Recipe_Load(object sender, EventArgs e)
        {
            comboRecipeNames.DataSource = GetRecipeNames();
            comboRecipeNames.DisplayMember = "recipe_name";
            comboIngredientName.DataSource = GetIngredientNames();
            comboIngredientName.DisplayMember = "ingredient_name";
            comboRecipeNames.Text = "";
            txtRecipeID.Text = "";
            comboIngredientName.Text = "";
            txtIngredientID.Text = "";
        }
        private DataTable GetRecipeNames()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT DISTINCT recipe_name FROM recipes";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }
        private DataTable GetIngredientNames()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = "SELECT DISTINCT ingredient_name FROM ingredients";
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }


        //Controlling the ID textboxes
        private int RecipeID()
        {
            int recipeID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT recipe_id FROM recipes WHERE recipe_name LIKE '{comboRecipeNames.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                recipeID = Convert.ToInt16(table.Rows[0]["recipe_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return recipeID;
        }
        private int IngredientID()
        {
            int ingredientID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT ingredient_id FROM ingredients WHERE ingredient_name LIKE '{comboIngredientName.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                ingredientID = Convert.ToInt16(table.Rows[0]["ingredient_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return ingredientID;
        }


        //Closing the form
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
