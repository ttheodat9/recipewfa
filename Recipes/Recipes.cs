﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.IO;
using System.Data.SqlClient;

namespace Recipes
{
    public partial class Recipes : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        int recipeID;
        string userInput;
        string imageName;
        string picPath;
        OpenFileDialog searchImages = new OpenFileDialog();


        public Recipes()
        {
            InitializeComponent();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            txtRecipeName.Text = "";
            txtRecipeDescription.Text = "";
            comboDifficulty.Text = "";
            pbPicture.Image = null;
            txtPrice.Text = "__.__";
            txtCategoryID.Text = "";
        }

        //Inserting the recipe
        private void BntnInsert_Click(object sender, EventArgs e)
        {
            if (txtRecipeName.Text != "" && txtRecipeDescription.Text != "" && comboDifficulty.Text != "" && txtPrice.Text != "" && txtCategoryID.Text != "")
            {
                DialogResult question = MessageBox.Show("Are you sure you want to insert this recipe?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO recipes(recipe_name, recipe_description, recipe_level_of_difficulty, recipe_final_price, category_id) VALUES ('{txtRecipeName.Text}', '{txtRecipeDescription.Text}', '{comboDifficulty.Text}', '{txtPrice.Text}', {txtCategoryID.Text})");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        bntnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Recipe successfully inserted!");
                    }
                    catch (SqlException)
                    {
                        string error = "An error occurred when performing the query or the recipe you requested to insert already exists.";
                        string title = "ERROR!";
                        MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;
                        DialogResult result = MessageBox.Show(error, title, buttons, MessageBoxIcon.Error);
                        connection.Close();
                    }
                    string getPicsCmd = $"UPDATE recipes SET recipe_photo = '{imageName}' WHERE recipe_name LIKE '{txtRecipeName.Text}'";
                    DataSet ds = PictureClass.ExecuteQuery(getPicsCmd);
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }

        //Updating the recipe
        private int GetRecipeID()
        {
            int recipeID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT recipe_id FROM recipes WHERE recipe_name LIKE '{txtRecipeName.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                recipeID = Convert.ToInt16(table.Rows[0]["recipe_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return recipeID;
        }
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (txtRecipeName.Text != "" && txtRecipeDescription.Text != "" && comboDifficulty.Text != "" && txtPrice.Text != "" && txtCategoryID.Text != "")
            {
                recipeID = GetRecipeID();
                DialogResult question = MessageBox.Show($"Are you sure you want to update Recipe {recipeID}?", "Update Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE recipes SET recipe_name = '{txtRecipeName.Text}', recipe_description = '{txtRecipeDescription.Text}', recipe_level_of_difficulty = '{comboDifficulty.Text}', recipe_final_price = '{txtPrice.Text}', category_id = {txtCategoryID.Text} WHERE recipe_id = {recipeID}");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Recipe successfully updated!");
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                    string getPicsCmd = $"UPDATE recipes SET recipe_photo = '{imageName}' WHERE recipe_name LIKE '{txtRecipeName.Text}'";
                    DataSet ds = PictureClass.ExecuteQuery(getPicsCmd);
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }

        //Getting the category ID
        private int GetCategoryID()
        {
            int categoryID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT category_id FROM categories WHERE category_name LIKE '{userInput}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                categoryID = Convert.ToInt16(table.Rows[0]["category_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return categoryID;
        }
        private void TxtCategoryID_Click(object sender, EventArgs e)
        {
            int CategoryID;
            string question = Interaction.InputBox($"Type the name of the category that this recipe belongs to:", "Finding the Category ID...");
            userInput = question;
            if (userInput != "")
            {
                CategoryID = GetCategoryID();
                txtCategoryID.Text = Convert.ToString(CategoryID);
            }
        }


        //Getting Pictures
        private void InitializeDialog()
        {
            searchImages.Filter = "Images|*.jpg;*.png;*.bmp";
            searchImages.Multiselect = true;
            searchImages.Title = "Search Images";
        }
        private void LoadPicture(PictureBox picboxToLoad)
        {
            InitializeDialog();
            string saveFileFolder = @"Images\";

            if (searchImages.ShowDialog() == DialogResult.OK)
            {
                picboxToLoad.Image = Image.FromFile(searchImages.FileName);
                string fullPictureUrl = searchImages.FileName;
                int picIndex = fullPictureUrl.LastIndexOf(@"\");
                imageName = (fullPictureUrl.Substring(picIndex)).Remove(0, 1); //Removes the character '\' from the result
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);
                picPath = fileFullPath + imageName;

                //Creates the directory to store pictures if it does not exist
                if (!Directory.Exists(fileFullPath))
                {
                    Directory.CreateDirectory(fileFullPath);
                }
                picboxToLoad.Image.Save(picPath);
            }
            else
            {
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);
                picPath = fileFullPath + imageName;
                picboxToLoad.Image.Save(picPath);
            }
        }
        private void BtnImage_Click(object sender, EventArgs e)
        {
            LoadPicture(pbPicture);
        }


        //Closing form
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
