﻿namespace Recipes
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recipesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientsToRecipesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.recipesToolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientsToRecipesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.reportsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recipesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.categoriesToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ingredientsToRecipesToolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.CornflowerBlue;
            this.menuStrip1.Font = new System.Drawing.Font("Footlight MT Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.reportsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 26);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStripMain";
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingredientToolStripMenuItem,
            this.recipesToolStripMenuItem,
            this.categoriesToolStripMenuItem,
            this.ingredientsToRecipesToolStripMenuItem});
            this.editToolStripMenuItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(45, 22);
            this.editToolStripMenuItem.Text = "Edit";
            // 
            // ingredientToolStripMenuItem
            // 
            this.ingredientToolStripMenuItem.BackColor = System.Drawing.Color.Beige;
            this.ingredientToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientToolStripMenuItem.Name = "ingredientToolStripMenuItem";
            this.ingredientToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.ingredientToolStripMenuItem.Text = "Ingredients";
            this.ingredientToolStripMenuItem.Click += new System.EventHandler(this.IngredientToolStripMenuItem_Click);
            // 
            // recipesToolStripMenuItem
            // 
            this.recipesToolStripMenuItem.BackColor = System.Drawing.Color.Beige;
            this.recipesToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGreen;
            this.recipesToolStripMenuItem.Name = "recipesToolStripMenuItem";
            this.recipesToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.recipesToolStripMenuItem.Text = "Recipes";
            this.recipesToolStripMenuItem.Click += new System.EventHandler(this.RecipesToolStripMenuItem_Click);
            // 
            // categoriesToolStripMenuItem
            // 
            this.categoriesToolStripMenuItem.BackColor = System.Drawing.Color.Beige;
            this.categoriesToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGreen;
            this.categoriesToolStripMenuItem.Name = "categoriesToolStripMenuItem";
            this.categoriesToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.categoriesToolStripMenuItem.Text = "Categories";
            this.categoriesToolStripMenuItem.Click += new System.EventHandler(this.CategoriesToolStripMenuItem_Click);
            // 
            // ingredientsToRecipesToolStripMenuItem
            // 
            this.ingredientsToRecipesToolStripMenuItem.BackColor = System.Drawing.Color.Beige;
            this.ingredientsToRecipesToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientsToRecipesToolStripMenuItem.Name = "ingredientsToRecipesToolStripMenuItem";
            this.ingredientsToRecipesToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.ingredientsToRecipesToolStripMenuItem.Text = "Ingredients to Recipes";
            this.ingredientsToRecipesToolStripMenuItem.Click += new System.EventHandler(this.IngredientsToRecipesToolStripMenuItem_Click);
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingredientsToolStripMenuItem1,
            this.recipesToolStripMenuItem3,
            this.categoriesToolStripMenuItem2,
            this.ingredientsToRecipesToolStripMenuItem1});
            this.searchToolStripMenuItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(64, 22);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // ingredientsToolStripMenuItem1
            // 
            this.ingredientsToolStripMenuItem1.BackColor = System.Drawing.Color.Beige;
            this.ingredientsToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientsToolStripMenuItem1.Name = "ingredientsToolStripMenuItem1";
            this.ingredientsToolStripMenuItem1.Size = new System.Drawing.Size(220, 22);
            this.ingredientsToolStripMenuItem1.Text = "Ingredients";
            this.ingredientsToolStripMenuItem1.Click += new System.EventHandler(this.IngredientsToolStripMenuItem1_Click);
            // 
            // recipesToolStripMenuItem3
            // 
            this.recipesToolStripMenuItem3.BackColor = System.Drawing.Color.Beige;
            this.recipesToolStripMenuItem3.ForeColor = System.Drawing.Color.DarkGreen;
            this.recipesToolStripMenuItem3.Name = "recipesToolStripMenuItem3";
            this.recipesToolStripMenuItem3.Size = new System.Drawing.Size(220, 22);
            this.recipesToolStripMenuItem3.Text = "Recipes";
            this.recipesToolStripMenuItem3.Click += new System.EventHandler(this.RecipesToolStripMenuItem3_Click);
            // 
            // categoriesToolStripMenuItem2
            // 
            this.categoriesToolStripMenuItem2.BackColor = System.Drawing.Color.Beige;
            this.categoriesToolStripMenuItem2.ForeColor = System.Drawing.Color.DarkGreen;
            this.categoriesToolStripMenuItem2.Name = "categoriesToolStripMenuItem2";
            this.categoriesToolStripMenuItem2.Size = new System.Drawing.Size(220, 22);
            this.categoriesToolStripMenuItem2.Text = "Categories";
            this.categoriesToolStripMenuItem2.Click += new System.EventHandler(this.CategoriesToolStripMenuItem2_Click);
            // 
            // ingredientsToRecipesToolStripMenuItem1
            // 
            this.ingredientsToRecipesToolStripMenuItem1.BackColor = System.Drawing.Color.Beige;
            this.ingredientsToRecipesToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientsToRecipesToolStripMenuItem1.Name = "ingredientsToRecipesToolStripMenuItem1";
            this.ingredientsToRecipesToolStripMenuItem1.Size = new System.Drawing.Size(220, 22);
            this.ingredientsToRecipesToolStripMenuItem1.Text = "Ingredients to Recipes";
            this.ingredientsToRecipesToolStripMenuItem1.Click += new System.EventHandler(this.IngredientsToRecipesToolStripMenuItem1_Click);
            // 
            // reportsToolStripMenuItem
            // 
            this.reportsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ingredientsToolStripMenuItem,
            this.recipesToolStripMenuItem2,
            this.categoriesToolStripMenuItem1,
            this.ingredientsToRecipesToolStripMenuItem2});
            this.reportsToolStripMenuItem.ForeColor = System.Drawing.Color.MidnightBlue;
            this.reportsToolStripMenuItem.Name = "reportsToolStripMenuItem";
            this.reportsToolStripMenuItem.Size = new System.Drawing.Size(70, 22);
            this.reportsToolStripMenuItem.Text = "Reports";
            // 
            // ingredientsToolStripMenuItem
            // 
            this.ingredientsToolStripMenuItem.BackColor = System.Drawing.Color.Beige;
            this.ingredientsToolStripMenuItem.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientsToolStripMenuItem.Name = "ingredientsToolStripMenuItem";
            this.ingredientsToolStripMenuItem.Size = new System.Drawing.Size(220, 22);
            this.ingredientsToolStripMenuItem.Text = "Ingredients";
            this.ingredientsToolStripMenuItem.Click += new System.EventHandler(this.IngredientsToolStripMenuItem_Click);
            // 
            // recipesToolStripMenuItem2
            // 
            this.recipesToolStripMenuItem2.BackColor = System.Drawing.Color.Beige;
            this.recipesToolStripMenuItem2.ForeColor = System.Drawing.Color.DarkGreen;
            this.recipesToolStripMenuItem2.Name = "recipesToolStripMenuItem2";
            this.recipesToolStripMenuItem2.Size = new System.Drawing.Size(220, 22);
            this.recipesToolStripMenuItem2.Text = "Recipes";
            this.recipesToolStripMenuItem2.Click += new System.EventHandler(this.RecipesToolStripMenuItem2_Click);
            // 
            // categoriesToolStripMenuItem1
            // 
            this.categoriesToolStripMenuItem1.BackColor = System.Drawing.Color.Beige;
            this.categoriesToolStripMenuItem1.ForeColor = System.Drawing.Color.DarkGreen;
            this.categoriesToolStripMenuItem1.Name = "categoriesToolStripMenuItem1";
            this.categoriesToolStripMenuItem1.Size = new System.Drawing.Size(220, 22);
            this.categoriesToolStripMenuItem1.Text = "Categories";
            this.categoriesToolStripMenuItem1.Click += new System.EventHandler(this.CategoriesToolStripMenuItem1_Click);
            // 
            // ingredientsToRecipesToolStripMenuItem2
            // 
            this.ingredientsToRecipesToolStripMenuItem2.BackColor = System.Drawing.Color.Beige;
            this.ingredientsToRecipesToolStripMenuItem2.ForeColor = System.Drawing.Color.DarkGreen;
            this.ingredientsToRecipesToolStripMenuItem2.Name = "ingredientsToRecipesToolStripMenuItem2";
            this.ingredientsToRecipesToolStripMenuItem2.Size = new System.Drawing.Size(220, 22);
            this.ingredientsToRecipesToolStripMenuItem2.Text = "Ingredients to Recipes";
            this.ingredientsToRecipesToolStripMenuItem2.Click += new System.EventHandler(this.IngredientsToRecipesToolStripMenuItem2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Ravie", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Bisque;
            this.label1.Location = new System.Drawing.Point(256, 110);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(283, 43);
            this.label1.TabIndex = 1;
            this.label1.Text = "WELCOME...";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.DarkOliveGreen;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Home";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingredientToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recipesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingredientsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem recipesToolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem reportsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingredientsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem recipesToolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem categoriesToolStripMenuItem1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem ingredientsToRecipesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ingredientsToRecipesToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ingredientsToRecipesToolStripMenuItem2;
    }
}

