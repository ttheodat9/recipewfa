﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Recipes
{
    public partial class Categories : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        int categoryID;
        public Categories()
        {
            InitializeComponent();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = "";
            txtCategoryDescription.Text = "";
            txtChargePerson.Text = "";
        }

        private void BntnInsert_Click(object sender, EventArgs e)
        {
            if (txtCategoryName.Text != "" && txtCategoryDescription.Text != "" && txtChargePerson.Text != "")
            {
                DialogResult question = MessageBox.Show("Are you sure you want to insert this category?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO categories(category_name, category_description, category_person_in_charge) VALUES ('{txtCategoryName.Text}', '{txtCategoryDescription.Text}', '{txtChargePerson.Text}')");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        bntnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Category successfully inserted!");
                    }
                    catch (SqlException)
                    {
                        string error = "An error occurred when performing the query or the category you requested to insert already exists.";
                        string title = "ERROR!";
                        MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;
                        DialogResult result = MessageBox.Show(error, title, buttons, MessageBoxIcon.Error);
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }


        private int GetCategoryID()
        {
            int categoryID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT category_id FROM categories WHERE category_name LIKE '{txtCategoryName.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                categoryID = Convert.ToInt16(table.Rows[0]["category_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return categoryID;
        }
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (txtCategoryName.Text != "" && txtCategoryDescription.Text != "" && txtChargePerson.Text != "")
            {
                categoryID = GetCategoryID();
                DialogResult question = MessageBox.Show($"Are you sure you want to update Category {categoryID}?", "Update Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE categories SET category_name = '{txtCategoryName.Text}', category_description = '{txtCategoryDescription.Text}', category_person_in_charge = '{txtChargePerson.Text}' WHERE category_id = {categoryID}");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Category successfully updated!");
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
