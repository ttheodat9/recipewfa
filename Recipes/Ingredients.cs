﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;


namespace Recipes
{
    public partial class Ingredients : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        int ingredientID;

        public Ingredients()
        {
            InitializeComponent();
        }

        private void BtnNew_Click(object sender, EventArgs e)
        {
            txtIngredientName.Text = "";
            txtRequiredAmount.Text = "__.__";
            comboUnits.Text = "";
            txtCurrentAmount.Text = "__.__";

        }

        private void BntnInsert_Click(object sender, EventArgs e)
        {
            if (txtIngredientName.Text != "" && txtRequiredAmount.Text != "" && comboUnits.Text != "" && txtCurrentAmount.Text != "")
            {
                DialogResult question = MessageBox.Show("Are you sure you want to insert this ingredient?", "Insert", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"INSERT INTO ingredients(ingredient_name, ingredient_required_quantity, ingredient_unit_of_measurement, ingredient_current_amount) VALUES ('{txtIngredientName.Text}', {txtRequiredAmount.Text}, '{comboUnits.Text}', {txtCurrentAmount.Text})");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        bntnInsert.Text = "Insert";
                        btnUpdate.Enabled = true;
                        MessageBox.Show("Ingredient successfully inserted!");
                    }
                    catch (SqlException)
                    {
                        string error = "an error occurred when performing the query or the ingredient you requested to insert already exists.";
                        string title = "error!";
                        MessageBoxButtons buttons = MessageBoxButtons.AbortRetryIgnore;
                        DialogResult result = MessageBox.Show(error, title, buttons, MessageBoxIcon.Error);
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }


        //Updating
        private void BtnUpdate_Click(object sender, EventArgs e)
        {
            if (txtIngredientName.Text != "" && txtRequiredAmount.Text != "" && comboUnits.Text != "" && txtCurrentAmount.Text != "")
            {
                ingredientID = ID();
                DialogResult question = MessageBox.Show($"Are you sure you want to update Ingredient {ingredientID}?", "Update Verification", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (question == DialogResult.Yes)
                {
                    try
                    {
                        connection.Open();
                        SqlCommand comm = connection.CreateCommand();
                        comm.CommandText = String.Format($"UPDATE ingredients SET ingredient_name = '{txtIngredientName.Text}', ingredient_required_quantity= '{txtRequiredAmount.Text}', ingredient_unit_of_measurement = '{comboUnits.Text}', ingredient_current_amount = '{txtCurrentAmount.Text}' WHERE ingredient_id = {ingredientID}");
                        comm.ExecuteNonQuery();
                        connection.Close();
                        MessageBox.Show("Ingredient successfully updated!");
                    }
                    catch (SqlException)
                    {
                        MessageBox.Show("An error occurred when performing the query");
                        connection.Close();
                    }
                }
            }
            else
            {
                MessageBox.Show("Empty form!");
            }
        }
        private int ID()
        {
            int ingredientID = 0;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                comm.CommandText = String.Format($"SELECT ingredient_id FROM ingredients WHERE ingredient_name LIKE '{txtIngredientName.Text}'");
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                DataTable table = new DataTable();
                adapter.Fill(table);
                ingredientID = Convert.ToInt16(table.Rows[0]["ingredient_id"].ToString());
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return ingredientID;
        }


        //Closing the Program
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }

}
