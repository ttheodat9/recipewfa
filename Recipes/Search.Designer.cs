﻿namespace Recipes
{
    partial class Search
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvAll = new System.Windows.Forms.DataGridView();
            this.comboSearchAll = new System.Windows.Forms.ComboBox();
            this.labelSearchAll = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvAll
            // 
            this.dgvAll.AllowUserToAddRows = false;
            this.dgvAll.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAll.BackgroundColor = System.Drawing.Color.Beige;
            this.dgvAll.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableWithoutHeaderText;
            this.dgvAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAll.Location = new System.Drawing.Point(12, 197);
            this.dgvAll.Name = "dgvAll";
            this.dgvAll.RowHeadersVisible = false;
            this.dgvAll.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAll.Size = new System.Drawing.Size(748, 150);
            this.dgvAll.TabIndex = 0;
            // 
            // comboSearchAll
            // 
            this.comboSearchAll.BackColor = System.Drawing.SystemColors.Info;
            this.comboSearchAll.FormattingEnabled = true;
            this.comboSearchAll.Location = new System.Drawing.Point(438, 139);
            this.comboSearchAll.Name = "comboSearchAll";
            this.comboSearchAll.Size = new System.Drawing.Size(297, 21);
            this.comboSearchAll.TabIndex = 1;
            this.comboSearchAll.SelectedIndexChanged += new System.EventHandler(this.ComboSearchAll_SelectedIndexChanged);
            // 
            // labelSearchAll
            // 
            this.labelSearchAll.AutoSize = true;
            this.labelSearchAll.BackColor = System.Drawing.SystemColors.Info;
            this.labelSearchAll.Font = new System.Drawing.Font("Ravie", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSearchAll.ForeColor = System.Drawing.Color.LightCoral;
            this.labelSearchAll.Location = new System.Drawing.Point(30, 139);
            this.labelSearchAll.Name = "labelSearchAll";
            this.labelSearchAll.Size = new System.Drawing.Size(0, 22);
            this.labelSearchAll.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.Info;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(348, 375);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 44);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // Search
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(772, 450);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.labelSearchAll);
            this.Controls.Add(this.comboSearchAll);
            this.Controls.Add(this.dgvAll);
            this.Name = "Search";
            this.Text = "Search";
            this.Load += new System.EventHandler(this.Search_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAll)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvAll;
        private System.Windows.Forms.ComboBox comboSearchAll;
        private System.Windows.Forms.Label labelSearchAll;
        private System.Windows.Forms.Button btnClose;
    }
}