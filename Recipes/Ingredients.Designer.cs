﻿namespace Recipes
{
    partial class Ingredients
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ingredients));
            this.groupBoxIngredients = new System.Windows.Forms.GroupBox();
            this.txtCurrentAmount = new System.Windows.Forms.MaskedTextBox();
            this.txtRequiredAmount = new System.Windows.Forms.MaskedTextBox();
            this.comboUnits = new System.Windows.Forms.ComboBox();
            this.txtIngredientName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.bntnInsert = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBoxIngredients.SuspendLayout();
            this.groupBoxOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxIngredients
            // 
            this.groupBoxIngredients.BackColor = System.Drawing.Color.Linen;
            this.groupBoxIngredients.Controls.Add(this.txtCurrentAmount);
            this.groupBoxIngredients.Controls.Add(this.txtRequiredAmount);
            this.groupBoxIngredients.Controls.Add(this.comboUnits);
            this.groupBoxIngredients.Controls.Add(this.txtIngredientName);
            this.groupBoxIngredients.Controls.Add(this.label4);
            this.groupBoxIngredients.Controls.Add(this.label3);
            this.groupBoxIngredients.Controls.Add(this.label2);
            this.groupBoxIngredients.Controls.Add(this.label1);
            this.groupBoxIngredients.Font = new System.Drawing.Font("Tw Cen MT", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxIngredients.ForeColor = System.Drawing.Color.Maroon;
            this.groupBoxIngredients.Location = new System.Drawing.Point(250, 40);
            this.groupBoxIngredients.Name = "groupBoxIngredients";
            this.groupBoxIngredients.Size = new System.Drawing.Size(352, 190);
            this.groupBoxIngredients.TabIndex = 2;
            this.groupBoxIngredients.TabStop = false;
            this.groupBoxIngredients.Text = "Ingredients...";
            // 
            // txtCurrentAmount
            // 
            this.txtCurrentAmount.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtCurrentAmount.Location = new System.Drawing.Point(153, 148);
            this.txtCurrentAmount.Mask = "00.00";
            this.txtCurrentAmount.Name = "txtCurrentAmount";
            this.txtCurrentAmount.Size = new System.Drawing.Size(48, 25);
            this.txtCurrentAmount.TabIndex = 4;
            // 
            // txtRequiredAmount
            // 
            this.txtRequiredAmount.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtRequiredAmount.Location = new System.Drawing.Point(153, 67);
            this.txtRequiredAmount.Mask = "00.00";
            this.txtRequiredAmount.Name = "txtRequiredAmount";
            this.txtRequiredAmount.Size = new System.Drawing.Size(48, 25);
            this.txtRequiredAmount.TabIndex = 2;
            // 
            // comboUnits
            // 
            this.comboUnits.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.comboUnits.FormattingEnabled = true;
            this.comboUnits.Items.AddRange(new object[] {
            "grams (g)",
            "liters (l)",
            "pounds (lbs)",
            "tablespoons (tbsp)",
            "ounces (oz)",
            "fluid ounces (fl oz)",
            "quarts (qt)",
            "gallons (gal)",
            "pints (pt)",
            "dozen (doz)"});
            this.comboUnits.Location = new System.Drawing.Point(153, 110);
            this.comboUnits.Name = "comboUnits";
            this.comboUnits.Size = new System.Drawing.Size(180, 27);
            this.comboUnits.TabIndex = 3;
            // 
            // txtIngredientName
            // 
            this.txtIngredientName.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.txtIngredientName.Location = new System.Drawing.Point(61, 31);
            this.txtIngredientName.Name = "txtIngredientName";
            this.txtIngredientName.Size = new System.Drawing.Size(243, 25);
            this.txtIngredientName.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AllowDrop = true;
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 148);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 19);
            this.label4.TabIndex = 5;
            this.label4.Text = "Current Amount:";
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 110);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Unit of Measurement:";
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Required Quantity:";
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOptions.Controls.Add(this.btnClose);
            this.groupBoxOptions.Controls.Add(this.btnUpdate);
            this.groupBoxOptions.Controls.Add(this.bntnInsert);
            this.groupBoxOptions.Controls.Add(this.btnNew);
            this.groupBoxOptions.Font = new System.Drawing.Font("Tw Cen MT", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOptions.ForeColor = System.Drawing.Color.Maroon;
            this.groupBoxOptions.Location = new System.Drawing.Point(275, 271);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(302, 158);
            this.groupBoxOptions.TabIndex = 14;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options...";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Linen;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(103, 106);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 44);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Linen;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(207, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(72, 44);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // bntnInsert
            // 
            this.bntnInsert.BackColor = System.Drawing.Color.Linen;
            this.bntnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bntnInsert.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntnInsert.Location = new System.Drawing.Point(107, 47);
            this.bntnInsert.Name = "bntnInsert";
            this.bntnInsert.Size = new System.Drawing.Size(69, 44);
            this.bntnInsert.TabIndex = 5;
            this.bntnInsert.Text = "Insert";
            this.bntnInsert.UseVisualStyleBackColor = false;
            this.bntnInsert.Click += new System.EventHandler(this.BntnInsert_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Linen;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(6, 47);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(71, 44);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // Ingredients
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBoxOptions);
            this.Controls.Add(this.groupBoxIngredients);
            this.Name = "Ingredients";
            this.Text = "Ingredients";
            this.groupBoxIngredients.ResumeLayout(false);
            this.groupBoxIngredients.PerformLayout();
            this.groupBoxOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxIngredients;
        private System.Windows.Forms.ComboBox comboUnits;
        private System.Windows.Forms.TextBox txtIngredientName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.MaskedTextBox txtCurrentAmount;
        private System.Windows.Forms.MaskedTextBox txtRequiredAmount;
        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button bntnInsert;
        private System.Windows.Forms.Button btnNew;
    }
}