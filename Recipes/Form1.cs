﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Recipes
{
    public partial class MainForm : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");

        //Background images for Search and Reports Pages
        Image ingredientImage = new Bitmap(@"D:\SeniorYearProgramming(Fall)\SQL\Recipes System (3)\ingredients.jpg");
        Image recipeImage = new Bitmap(@"D:\SeniorYearProgramming(Fall)\SQL\Recipes System (3)\recipes.jpg");
        Image categoryImage = new Bitmap(@"D:\SeniorYearProgramming(Fall)\SQL\Recipes System (3)\category.jpg");
        Image recipeIngredientImage = new Bitmap(@"D:\SeniorYearProgramming(Fall)\SQL\Recipes System (3)\i-r.jpg");

        public MainForm()
        {
            InitializeComponent();
        }

        //Loading Edit Page
        private void IngredientToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ingredients ingredients = new Ingredients();
            ingredients.Show();
        }
        private void RecipesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Recipes recipes = new Recipes();
            recipes.Show();
        }
        private void CategoriesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Categories categories = new Categories();
            categories.Show();
        }
        private void IngredientsToRecipesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Ingredient_Recipe ingredient_Recipe = new Ingredient_Recipe();
            ingredient_Recipe.Show();
        }

        //Loading Search Page
        private void IngredientsToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Search search = new Search();
            search.ingredientPage = true;
            search.Show();
            search.BackgroundImage = ingredientImage;
        }
        private void RecipesToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            Search search = new Search();
            search.recipePage = true;
            search.Show();
            search.BackgroundImage = recipeImage;
        }
        private void CategoriesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Search search = new Search();
            search.categoryPage = true;
            search.Show();
            search.BackgroundImage = categoryImage;
        }
        private void IngredientsToRecipesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Search search = new Search();
            search.recipeIngredientPage = true;
            search.Show();
            search.BackgroundImage = recipeIngredientImage;
        }


        //Loading Report Page
        private void IngredientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();
            reports.ingredientReport = true;
            reports.Show();
            reports.BackgroundImage = ingredientImage;
        }
        private void RecipesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();
            reports.recipeReport = true;
            reports.Show();
            reports.BackgroundImage = recipeImage;
        }
        private void CategoriesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();
            reports.categoryReport = true;
            reports.Show();
            reports.BackgroundImage = categoryImage;
        }
        private void IngredientsToRecipesToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            Reports reports = new Reports();
            reports.recipeIngredientReport = true;
            reports.Show();
            reports.BackgroundImage = recipeIngredientImage;
        }
    }
}
