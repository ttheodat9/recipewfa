﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Recipes
{
    public partial class Search : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        public bool ingredientPage = false;
        public bool recipePage = false;
        public bool categoryPage = false;
        public bool recipeIngredientPage = false;

        public Search()
        {
            InitializeComponent();
        }


        //Showing the recipe images
        private void ShowAllPictures()
        {
            try
            {
                dgvAll.DataSource = null;
                //Read all pictures
                string getAllPicsCmd = $"SELECT * FROM recipes WHERE recipe_name LIKE '{comboSearchAll.Text}'";
                DataSet ds = PictureClass.ExecuteQuery(getAllPicsCmd);
                dgvAll.DataSource = ds.Tables[0];

                int lastDgvColumn = dgvAll.ColumnCount; //Gets the last columns number

                //Add an Image Column to the DataGridView at position zero (0).
                DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
                imageColumn.DataPropertyName = "Data";
                imageColumn.HeaderText = "recipe_photo";
                imageColumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                dgvAll.Columns.Insert(lastDgvColumn, imageColumn);
                dgvAll.RowTemplate.Height = 64;
                dgvAll.Columns[lastDgvColumn].Width = 64;

                string saveFileFolder = @"Images\";
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);

                //Add a new Byte[] Column.
                ds.Tables[0].Columns.Add("Data", Type.GetType("System.Byte[]"));

                //Convert all Images to Byte[] and copy to DataTable.
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string picPath = fileFullPath + @"\" + row["recipe_photo"].ToString();

                    row["Data"] = File.ReadAllBytes(picPath);
                }

                dgvAll.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error: " + ex.Message);
            }
        }

        //Loading different searches based on boolean results
        private void Search_Load(object sender, EventArgs e)
        {
            if (ingredientPage.ToString() == "True")
            {
                btnClose.ForeColor = Color.Maroon;
                labelSearchAll.ForeColor = Color.Maroon;
                labelSearchAll.Text = "Search an ingredient by name:";
                comboSearchAll.DataSource = GetAll();
                comboSearchAll.DisplayMember = "ingredient_name";
                ShowAll();
            }
            else if (recipePage.ToString() == "True")
            {
                btnClose.ForeColor = Color.Navy;
                labelSearchAll.ForeColor = Color.Navy;
                labelSearchAll.Text = "Search a recipe by name:";
                comboSearchAll.DataSource = GetAll();
                comboSearchAll.DisplayMember = "recipe_name";
                ShowAll();
            }
            else if (categoryPage.ToString() == "True")
            {
                btnClose.ForeColor = Color.DarkGreen;
                labelSearchAll.ForeColor = Color.DarkGreen;
                labelSearchAll.Text = "Search a category by name:";
                comboSearchAll.DataSource = GetAll();
                comboSearchAll.DisplayMember = "category_name";
                ShowAll();
            }
            else
            {
                btnClose.ForeColor = Color.OrangeRed;
                labelSearchAll.ForeColor = Color.OrangeRed;
                labelSearchAll.Text = "Search a record by combination:";
                comboSearchAll.DataSource = GetAll();
                comboSearchAll.DisplayMember = "Records";
                ShowAll();
            }
            
        }


        //Displaying search results based on boolean results
        public void ShowAll()
        {
            try
            {
                string queryStr;
                if (ingredientPage.ToString() == "True")
                {
                    queryStr = $"SELECT * FROM ingredients WHERE ingredient_name LIKE '{comboSearchAll.Text}'";
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = queryStr;
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                    connection.Close();
                }
                else if (recipePage.ToString() == "True")
                {
                    connection.Open();
                    ShowAllPictures();
                    connection.Close();
                }
                else if (categoryPage.ToString() == "True")
                {
                    queryStr = $"SELECT * FROM categories WHERE category_name LIKE '{comboSearchAll.Text}'";
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = queryStr;
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                    connection.Close();
                }
                if(recipeIngredientPage.ToString() == "True")
                {
                    queryStr = $"SELECT * FROM records WHERE (recipe_name + ' -> ' + ingredient_name) LIKE '{comboSearchAll.Text}'";
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    comm.CommandText = queryStr;
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    DataTable table = new DataTable();
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                connection.Close();
            }
        }


        //Showing options for the combo box based on boolean results
        private DataTable GetAll()
        {
            DataTable table = null;
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                if (ingredientPage.ToString() == "True")
                {
                    comm.CommandText = "SELECT DISTINCT ingredient_name FROM ingredients";
                }
                else if (recipePage.ToString() == "True")
                {
                    comm.CommandText = "SELECT DISTINCT recipe_name FROM recipes";
                }
                else if (categoryPage.ToString() == "True")
                {
                    comm.CommandText = "SELECT DISTINCT category_name FROM categories";
                }
                 else
                {
                    comm.CommandText = "SELECT DISTINCT (recipe_name + ' -> ' + ingredient_name) AS Records FROM records";
                }
                SqlDataAdapter adapter = new SqlDataAdapter(comm);
                table = new DataTable();
                adapter.Fill(table);
                connection.Close();
            }
            catch (SqlException)
            {
                MessageBox.Show("An error occurred when performing the query");
                connection.Close();
            }
            return table;
        }

        private void ComboSearchAll_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowAll();
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
