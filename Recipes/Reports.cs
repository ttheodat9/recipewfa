﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace Recipes
{
    public partial class Reports : Form
    {
        SqlConnection connection = new SqlConnection(@"Data Source=ZCM-233203639\SQLEXPRESS;Initial Catalog=manage_menu;Integrated Security=True");
        public bool ingredientReport = false;
        public bool recipeReport = false;
        public bool categoryReport = false;
        public bool recipeIngredientReport = false;
        int selectedID;

        public Reports()
        {
            InitializeComponent();
        }

        //Showing the recipe images
        private void ShowAllPictures()
        {
            try
            {
                dgvAll.DataSource = null;
                //Read all pictures
                string getAllPicsCmd = "SELECT * FROM recipes";
                DataSet ds = PictureClass.ExecuteQuery(getAllPicsCmd);
                dgvAll.DataSource = ds.Tables[0];

                int lastDgvColumn = dgvAll.ColumnCount; //Gets the last columns number

                //Add an Image Column to the DataGridView at position zero (0).
                DataGridViewImageColumn imageColumn = new DataGridViewImageColumn();
                imageColumn.DataPropertyName = "Data";
                imageColumn.HeaderText = "recipe_photo";
                imageColumn.ImageLayout = DataGridViewImageCellLayout.Stretch;
                dgvAll.Columns.Insert(lastDgvColumn, imageColumn);
                dgvAll.RowTemplate.Height = 64;
                dgvAll.Columns[lastDgvColumn].Width = 64;

                string saveFileFolder = @"Images\";
                string fileFullPath = Path.Combine(Application.StartupPath, saveFileFolder);

                //Add a new Byte[] Column.
                ds.Tables[0].Columns.Add("Data", Type.GetType("System.Byte[]"));

                //Convert all Images to Byte[] and copy to DataTable.
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    string picPath = fileFullPath + @"\" + row["recipe_photo"].ToString();

                    row["Data"] = File.ReadAllBytes(picPath);
                }
                dgvAll.DataSource = ds.Tables[0];
            }
            catch (Exception ex)
            {
                MessageBox.Show("There was an error: " + ex.Message);
            }
        }

        //Loading Reports based on boolean results
        private void LoadAll()
        {
            try
            {
                connection.Open();
                SqlCommand comm = connection.CreateCommand();
                if (ingredientReport.ToString() == "True")
                {
                    btnDelete.ForeColor = Color.Maroon;
                    btnClose.ForeColor = Color.Maroon;
                    labelReports.ForeColor = Color.Maroon;
                    labelReports.Text = "Ingredients...";
                    comm.CommandText = "SELECT * FROM ingredients";
                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                }
                else if (recipeReport.ToString() == "True")
                {
                    btnDelete.ForeColor = Color.Navy;
                    btnClose.ForeColor = Color.Navy;
                    labelReports.ForeColor = Color.Navy;
                    labelReports.Text = "Recipes...";
                    ShowAllPictures();
                }
                else if (categoryReport.ToString() == "True")
                {
                    btnDelete.ForeColor = Color.DarkGreen;
                    btnClose.ForeColor = Color.DarkGreen;
                    labelReports.ForeColor = Color.DarkGreen;
                    labelReports.Text = "Categories...";
                    comm.CommandText = "SELECT * FROM categories";
                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                }
                else
                {
                    btnDelete.ForeColor = Color.OrangeRed;
                    btnClose.ForeColor = Color.OrangeRed;
                    labelReports.ForeColor = Color.OrangeRed;
                    labelReports.Text = "Ingredients-Recipes Records...";
                    comm.CommandText = "SELECT * FROM records";
                    DataTable table = new DataTable();
                    SqlDataAdapter adapter = new SqlDataAdapter(comm);
                    adapter.Fill(table);
                    dgvAll.DataSource = table;
                }
                
                connection.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                connection.Close();
            }
        }
        private void Reports_Load(object sender, EventArgs e)
        {
            LoadAll();
        }

        //Selecting row ID
        private void DgvAll_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            selectedID = Convert.ToInt16(dgvAll.Rows[e.RowIndex].Cells[0].Value.ToString());
        }

        //Deleting row
        private void BtnDelete_Click(object sender, EventArgs e)
        {
            DialogResult questionDelete = MessageBox.Show($"Are you sure you want to delete this record, ID = {selectedID}?", "Delete", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (questionDelete == DialogResult.Yes)
            {
                try
                {
                    connection.Open();
                    SqlCommand comm = connection.CreateCommand();
                    if (ingredientReport.ToString() == "True")
                    {
                        comm.CommandText = String.Format($"DELETE FROM ingredients WHERE ingredient_id = {selectedID}");

                    }
                    else if (recipeReport.ToString() == "True")
                    {
                        comm.CommandText = String.Format($"DELETE FROM recipes WHERE recipe_id = {selectedID}");
                    }
                    else if (categoryReport.ToString() == "True")
                    {
                        comm.CommandText = String.Format($"DELETE FROM categories WHERE category_id = {selectedID}");
                    }
                    else
                    {
                        comm.CommandText = String.Format($"DELETE FROM records WHERE record_id = {selectedID}");
                    }
                    comm.ExecuteNonQuery();
                    connection.Close();
                    LoadAll();
                    MessageBox.Show("Record successfully deleted!");

                }
                catch (SqlException)
                {
                    MessageBox.Show("An error occurred when performing the query");
                    connection.Close();
                }
            }
        }

        //Closing form
        private void BtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
