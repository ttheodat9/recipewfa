﻿namespace Recipes
{
    partial class Categories
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Categories));
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.bntnInsert = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBoxCategories = new System.Windows.Forms.GroupBox();
            this.txtChargePerson = new System.Windows.Forms.TextBox();
            this.txtCategoryDescription = new System.Windows.Forms.RichTextBox();
            this.txtCategoryName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxOptions.SuspendLayout();
            this.groupBoxCategories.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOptions.Controls.Add(this.btnClose);
            this.groupBoxOptions.Controls.Add(this.btnUpdate);
            this.groupBoxOptions.Controls.Add(this.bntnInsert);
            this.groupBoxOptions.Controls.Add(this.btnNew);
            this.groupBoxOptions.Font = new System.Drawing.Font("Tw Cen MT", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOptions.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBoxOptions.Location = new System.Drawing.Point(156, 172);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(289, 158);
            this.groupBoxOptions.TabIndex = 1;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options...";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Bisque;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(103, 106);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 44);
            this.btnClose.TabIndex = 14;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Bisque;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(199, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(72, 44);
            this.btnUpdate.TabIndex = 13;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // bntnInsert
            // 
            this.bntnInsert.BackColor = System.Drawing.Color.Bisque;
            this.bntnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bntnInsert.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntnInsert.Location = new System.Drawing.Point(103, 47);
            this.bntnInsert.Name = "bntnInsert";
            this.bntnInsert.Size = new System.Drawing.Size(69, 44);
            this.bntnInsert.TabIndex = 12;
            this.bntnInsert.Text = "Insert";
            this.bntnInsert.UseVisualStyleBackColor = false;
            this.bntnInsert.Click += new System.EventHandler(this.BntnInsert_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Bisque;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(6, 47);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(71, 44);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // groupBoxCategories
            // 
            this.groupBoxCategories.BackColor = System.Drawing.Color.Bisque;
            this.groupBoxCategories.Controls.Add(this.txtChargePerson);
            this.groupBoxCategories.Controls.Add(this.txtCategoryDescription);
            this.groupBoxCategories.Controls.Add(this.txtCategoryName);
            this.groupBoxCategories.Controls.Add(this.label3);
            this.groupBoxCategories.Controls.Add(this.label2);
            this.groupBoxCategories.Controls.Add(this.label1);
            this.groupBoxCategories.Font = new System.Drawing.Font("Tw Cen MT", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxCategories.ForeColor = System.Drawing.Color.DarkGreen;
            this.groupBoxCategories.Location = new System.Drawing.Point(110, 346);
            this.groupBoxCategories.Name = "groupBoxCategories";
            this.groupBoxCategories.Size = new System.Drawing.Size(383, 169);
            this.groupBoxCategories.TabIndex = 4;
            this.groupBoxCategories.TabStop = false;
            this.groupBoxCategories.Text = "Categories...";
            // 
            // txtChargePerson
            // 
            this.txtChargePerson.BackColor = System.Drawing.SystemColors.Info;
            this.txtChargePerson.Location = new System.Drawing.Point(128, 124);
            this.txtChargePerson.Name = "txtChargePerson";
            this.txtChargePerson.Size = new System.Drawing.Size(238, 25);
            this.txtChargePerson.TabIndex = 11;
            // 
            // txtCategoryDescription
            // 
            this.txtCategoryDescription.BackColor = System.Drawing.SystemColors.Info;
            this.txtCategoryDescription.Location = new System.Drawing.Point(103, 59);
            this.txtCategoryDescription.Name = "txtCategoryDescription";
            this.txtCategoryDescription.Size = new System.Drawing.Size(263, 52);
            this.txtCategoryDescription.TabIndex = 10;
            this.txtCategoryDescription.Text = "";
            // 
            // txtCategoryName
            // 
            this.txtCategoryName.BackColor = System.Drawing.SystemColors.Info;
            this.txtCategoryName.Location = new System.Drawing.Point(70, 28);
            this.txtCategoryName.Name = "txtCategoryName";
            this.txtCategoryName.Size = new System.Drawing.Size(296, 25);
            this.txtCategoryName.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Person in Charge:";
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 19);
            this.label2.TabIndex = 3;
            this.label2.Text = "Description:";
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Name:";
            // 
            // Categories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(602, 527);
            this.Controls.Add(this.groupBoxCategories);
            this.Controls.Add(this.groupBoxOptions);
            this.Name = "Categories";
            this.Text = "Categories";
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxCategories.ResumeLayout(false);
            this.groupBoxCategories.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button bntnInsert;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBoxCategories;
        private System.Windows.Forms.RichTextBox txtCategoryDescription;
        private System.Windows.Forms.TextBox txtCategoryName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtChargePerson;
    }
}