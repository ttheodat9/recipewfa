﻿namespace Recipes
{
    partial class Ingredient_Recipe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Ingredient_Recipe));
            this.groupBoxOptions = new System.Windows.Forms.GroupBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.bntnInsert = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.groupBoxIR = new System.Windows.Forms.GroupBox();
            this.comboIngredientName = new System.Windows.Forms.ComboBox();
            this.comboRecipeNames = new System.Windows.Forms.ComboBox();
            this.txtIngredientID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRecipeID = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxOptions.SuspendLayout();
            this.groupBoxIR.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxOptions
            // 
            this.groupBoxOptions.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxOptions.Controls.Add(this.btnClose);
            this.groupBoxOptions.Controls.Add(this.btnUpdate);
            this.groupBoxOptions.Controls.Add(this.bntnInsert);
            this.groupBoxOptions.Controls.Add(this.btnNew);
            this.groupBoxOptions.Font = new System.Drawing.Font("Tw Cen MT", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxOptions.ForeColor = System.Drawing.Color.OrangeRed;
            this.groupBoxOptions.Location = new System.Drawing.Point(12, 141);
            this.groupBoxOptions.Name = "groupBoxOptions";
            this.groupBoxOptions.Size = new System.Drawing.Size(302, 158);
            this.groupBoxOptions.TabIndex = 16;
            this.groupBoxOptions.TabStop = false;
            this.groupBoxOptions.Text = "Options...";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Linen;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnClose.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Location = new System.Drawing.Point(103, 106);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(71, 44);
            this.btnClose.TabIndex = 7;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.Linen;
            this.btnUpdate.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnUpdate.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Location = new System.Drawing.Point(207, 47);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(72, 44);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.BtnUpdate_Click);
            // 
            // bntnInsert
            // 
            this.bntnInsert.BackColor = System.Drawing.Color.Linen;
            this.bntnInsert.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.bntnInsert.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bntnInsert.Location = new System.Drawing.Point(107, 47);
            this.bntnInsert.Name = "bntnInsert";
            this.bntnInsert.Size = new System.Drawing.Size(69, 44);
            this.bntnInsert.TabIndex = 5;
            this.bntnInsert.Text = "Insert";
            this.bntnInsert.UseVisualStyleBackColor = false;
            this.bntnInsert.Click += new System.EventHandler(this.BntnInsert_Click);
            // 
            // btnNew
            // 
            this.btnNew.BackColor = System.Drawing.Color.Linen;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnNew.Font = new System.Drawing.Font("Modern No. 20", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNew.Location = new System.Drawing.Point(6, 47);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(71, 44);
            this.btnNew.TabIndex = 0;
            this.btnNew.Text = "New";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.BtnNew_Click);
            // 
            // groupBoxIR
            // 
            this.groupBoxIR.BackColor = System.Drawing.Color.Linen;
            this.groupBoxIR.Controls.Add(this.comboIngredientName);
            this.groupBoxIR.Controls.Add(this.comboRecipeNames);
            this.groupBoxIR.Controls.Add(this.txtIngredientID);
            this.groupBoxIR.Controls.Add(this.label2);
            this.groupBoxIR.Controls.Add(this.txtRecipeID);
            this.groupBoxIR.Controls.Add(this.label6);
            this.groupBoxIR.Controls.Add(this.label3);
            this.groupBoxIR.Controls.Add(this.label1);
            this.groupBoxIR.Font = new System.Drawing.Font("Tw Cen MT", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxIR.ForeColor = System.Drawing.Color.OrangeRed;
            this.groupBoxIR.Location = new System.Drawing.Point(350, 42);
            this.groupBoxIR.Name = "groupBoxIR";
            this.groupBoxIR.Size = new System.Drawing.Size(414, 190);
            this.groupBoxIR.TabIndex = 15;
            this.groupBoxIR.TabStop = false;
            this.groupBoxIR.Text = "Ingredient to Recipe...";
            // 
            // comboIngredientName
            // 
            this.comboIngredientName.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.comboIngredientName.FormattingEnabled = true;
            this.comboIngredientName.ItemHeight = 19;
            this.comboIngredientName.Location = new System.Drawing.Point(128, 111);
            this.comboIngredientName.Name = "comboIngredientName";
            this.comboIngredientName.Size = new System.Drawing.Size(257, 27);
            this.comboIngredientName.TabIndex = 20;
            // 
            // comboRecipeNames
            // 
            this.comboRecipeNames.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.comboRecipeNames.FormattingEnabled = true;
            this.comboRecipeNames.Location = new System.Drawing.Point(127, 28);
            this.comboRecipeNames.Name = "comboRecipeNames";
            this.comboRecipeNames.Size = new System.Drawing.Size(257, 27);
            this.comboRecipeNames.TabIndex = 1;
            // 
            // txtIngredientID
            // 
            this.txtIngredientID.Location = new System.Drawing.Point(127, 149);
            this.txtIngredientID.Name = "txtIngredientID";
            this.txtIngredientID.ReadOnly = true;
            this.txtIngredientID.Size = new System.Drawing.Size(29, 25);
            this.txtIngredientID.TabIndex = 4;
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 152);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 19);
            this.label2.TabIndex = 17;
            this.label2.Text = "Ingredient ID:";
            // 
            // txtRecipeID
            // 
            this.txtRecipeID.Location = new System.Drawing.Point(128, 72);
            this.txtRecipeID.Name = "txtRecipeID";
            this.txtRecipeID.ReadOnly = true;
            this.txtRecipeID.Size = new System.Drawing.Size(29, 25);
            this.txtRecipeID.TabIndex = 2;
            // 
            // label6
            // 
            this.label6.AllowDrop = true;
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 19);
            this.label6.TabIndex = 15;
            this.label6.Text = "Recipe ID:";
            // 
            // label3
            // 
            this.label3.AllowDrop = true;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 19);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ingredient Name:";
            // 
            // label1
            // 
            this.label1.AllowDrop = true;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 19);
            this.label1.TabIndex = 2;
            this.label1.Text = "Recipe Name:";
            // 
            // Ingredient_Recipe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBoxOptions);
            this.Controls.Add(this.groupBoxIR);
            this.Name = "Ingredient_Recipe";
            this.Text = "Ingredient_Recipe";
            this.Load += new System.EventHandler(this.Ingredient_Recipe_Load);
            this.groupBoxOptions.ResumeLayout(false);
            this.groupBoxIR.ResumeLayout(false);
            this.groupBoxIR.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxOptions;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button bntnInsert;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.GroupBox groupBoxIR;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtIngredientID;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtRecipeID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboIngredientName;
        private System.Windows.Forms.ComboBox comboRecipeNames;
    }
}